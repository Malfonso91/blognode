var express = require("express"),
    handlebars = require('express-handlebars');
 
var bodyParser = require('body-parser'); 
var app = express();

app.use(bodyParser.json({ type: 'application/json' }))
app.use(bodyParser.urlencoded({ extended: true }));
	
var isPosted = false;
var posts = [{
                "subject": "Post numero 1",
                "description": "Descripcion 1",
                "time": new Date()
            },
			
			{
                "subject": "Post numero 2",
                "description": "Descripcion 2",
                "time": new Date()
            },
			{
                "subject": "Post numero 3",
                "description": "Descripcion 3",
                "time": new Date()
            }];

			
 
app.engine('handlebars', handlebars());
app.set('view engine', 'handlebars');
app.set("views", "./views");
 
 //posts
app.get('/posts', function(req, res){
    res.render('posts', { "title": "Hola gente", "posts" : posts } );
});
app.post('/posts', function(req, res){
	var missatge = req.body;
    res.send(missatge);
});

//posts/new
app.get('/posts/new', function(req, res){
			var lastP = posts.length - 1;
			var obj = posts[lastP];
			var title = obj.subject;
			res.render('posts', {"title": title, "posts" : [obj]});
});






app.listen(8080);